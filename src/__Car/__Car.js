import React from "react"

// const car = () => (
//     <div>
//         <h2>This is car component</h2>
//         <p>This is a tex <strong>of component</strong></p>
//     </div>
// )


// const car = () => {
//     return (
//         <h2>This is car component</h2>
//     )
// }


// function Car() {
//     return (
//         <h2>This is car component</h2>
//     )
// }

// export default car

export default () => (
    <div>
        <h2>This is car component</h2>
        <p>This is a tex <strong>of component</strong></p>
    </div>
)