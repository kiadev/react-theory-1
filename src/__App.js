import React from 'react';
import logo from './logo.svg';
import './App.css';

const divStyle = {
    textAlign : 'center'
}

const App = () => {

    // return React.createElement(
    //     'div',
    //     {
    //         className: 'App'
    //     },
    //     React.createElement(
    //         'header',
    //         {
    //             className: 'App-header'
    //         },
    //         React.createElement(
    //             'img',
    //             {
    //                 src: logo,
    //                 className: 'App-logo',
    //                 alt: 'logo'
    //             }
    //         ),
    //         React.createElement(
    //             'p',
    //             null,
    //             'Edit '+ React.createElement('code', null, 'src/App.js') +' and save to reload.'
    //         ),
    //         React.createElement(
    //             'a',
    //             {
    //                 className: 'App-link',
    //                 href: 'https://reactjs.org',
    //                 target: '_blank',
    //                 rel: 'noopener noreferrer'
    //             },
    //             'Learn React'
    //         )
    //     )
    // )

    return (
        <div className="App" style={divStyle}>
            <header className="App-header">
                <h1 style={{color:'blue',fontSize:'20px'}}>Hello, World!!!</h1>
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
        </div>
    );
}

export default App;
