import React, {Component} from 'react'
import Car from './Car/Car'
import './App.css'
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary"
import Counter from "./Counter/Counter"

export const ClickContext = React.createContext(false)

class App extends Component{

    state = {
        cars: [
            {name:'Ford', year: 2018},
            {name:'Mercedes', year: 2016},
            {name:'Honda', year: 2015}
        ],
        pageTitle: 'React Component',
        showCars: false,
        clicked: false
    }

    changeTitleHandler = (newTitle) => {
        this.setState( {
            pageTitle: newTitle
        } )
    }

    taggleCarsHandler = () => {
        this.setState({
            showCars: !this.state.showCars
        })
    }

    handleInput = (event) => {
        this.setState({
            pageTitle: event.target.value
        })

    }

    onChangeName(name, index) {
        const car = this.state.cars[index]
        car.name = name
        const cars = [...this.state.cars]
        cars[index] = car
        this.setState({
            // cars: cars
            cars
        })
    }

    deleteHandler(index) {
        const cars = [...this.state.cars]
        cars.splice( index, 1 )
        this.setState({
            // cars: cars
            cars
        })
    }

    componentWillMount() {
        console.log('App componentWillMount')
    }

    componentDidMount() {
        console.log('App componentDidMount')
    }

    render() {

        console.log('App render')

        const cars = this.state.cars
        let carsOutput = null
        if ( this.state.showCars ) {
            carsOutput = cars.map( (car, index) => {
                return (
                    <ErrorBoundary key={index}>
                        <Car
                            name={car.name}
                            year={car.year}
                            onChangeName={(event) => this.onChangeName(event.target.value, index)}
                            onDelete={this.deleteHandler.bind(this, index)}
                            // onChangeTile={this.changeTitleHandler.bind(this, car.name)}
                            /*    // Более правильный способ*/
                            /*onChangeTile={ () => this.changeTitleHandler(car.name)}*/
                        />
                    </ErrorBoundary>

                )
            } )

        }

        return (
            <div className="App">
                <h1>{ this.state.pageTitle }</h1>
                <hr/>
                {/*<Counter clicked={this.state.clicked} />*/}
                <ClickContext.Provider value={this.state.clicked}>
                    <Counter />
                </ClickContext.Provider>
                <hr/>
                <input type="text" onChange={this.handleInput} /><br/>
                <button
                    onClick={this.taggleCarsHandler}
                    style={{marginTop:20}}
                >Toggle cars</button>

                <button onClick={() => this.setState({clicked: true})}>Change clicked</button>

                {/*{ this.state.showCars ? carsOutput : null }*/}
                <div style={{
                    width: 400,
                    margin: "auto",
                    paddingTop: "20px"
                }}>
                    {carsOutput}
                </div>

            </div>
        )
    }
}

export default App