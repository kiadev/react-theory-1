import React, {Component} from 'react'
import Car from './Car/Car'
import './App.css'

class App extends Component{

    state = {
        cars: [
            {name:'Ford', year: 2018},
            {name:'Mercedes', year: 2016},
            {name:'Honda', year: 2015}
        ],
        pageTitle: 'React Component',
        showCars: false
    }

    changeTitleHandler = (newTitle) => {
        this.setState( {
            pageTitle: newTitle
        } )
    }

    taggleCarsHandler = () => {
        this.setState({
            showCars: !this.state.showCars
        })
    }

    handleInput = (event) => {
        this.setState({
            pageTitle: event.target.value
        })

    }

    render() {

        const cars = this.state.cars
        let carsOutput = null
        if ( this.state.showCars ) {
            carsOutput = cars.map( (car, index) => {
                return (
                    <Car
                        key={index}
                        name={car.name}
                        year={car.year}
                        onChangeTile={this.changeTitleHandler.bind(this, car.name)}
                        /*    // Более правильный способ*/
                        /*onChangeTile={ () => this.changeTitleHandler(car.name)}*/
                    />
                )
            } )

        }

        return (
            <div className="App">
                <h1>{ this.state.pageTitle }</h1>

                <input type="text" onChange={this.handleInput} /><br/>

                <button
                    onClick={this.taggleCarsHandler}
                >Toggle cars</button>

                {/*{ this.state.showCars ? carsOutput : null }*/}
                {carsOutput}
            </div>
        )
    }
}

export default App