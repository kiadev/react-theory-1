import React, {Component} from 'react'
import Car from './Car/Car'
import './App.css'

class App extends Component{
    render() {

        return (
            <div className="App">
                <h1>Hello, World</h1>
                <Car name={'Ford'} year={2018}>
                    <p style={{color: 'blue'}}>COLOR</p>
                </Car>
                <Car name={'Mercedes'} year={2016} />
                <Car name={'Honda'} year={2015} />
                <Car name={'Honda'} year={2015}>
                    {/* Обращение к props.children */}
                    <p style={{color: 'red'}}>COLOR</p>
                </Car>
            </div>
        )
    }
}

export default App

// Working with __Car folder