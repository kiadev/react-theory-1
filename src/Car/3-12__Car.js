import React, {Component} from "react"
import Radium from "radium"

// import './Car.css';
import './Car.scss';
// import './Car.module.scss';

class Car extends Component {

    componentWillReceiveProps(nextProps, nextContext) {
        console.log('Car componentWillReceiveProps', nextProps)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('Car shouldComponentUpdate', nextProps, nextState)
        return nextProps.name.trim() !== this.props.name.trim()
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        console.log('Car componentWillUpdate', nextProps, nextState)
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     console.log('Car getDerivedStateFromProps', nextProps, prevState)
    //     return prevState
    // }
    //
    // getSnapshotBeforeUpdate(prevProps, prevState) {
    //     console.log('Car getSnapshotBeforeUpdate', prevProps, prevState)
    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('Car componentDidUpdate', prevProps, prevProps)
    }

    componentWillUnmount() {
        console.log('Car componentWillUnmount')
    }

    render() {

        console.log('Car render')

        // if (Math.random() > 0.7) {
        //     throw new Error('Car random failed')
        // }

        const inputClass = ['input']

        if (this.props.name !== '') {
            inputClass.push('green')
        } else {
            inputClass.push('red')
        }

        if (this.props.name.length > 4) {
            inputClass.push('bold')
        }

        const style = {
            border: "1px solid #ccc",
            boxShadow: "0 4px 5px 0 rgba(0,0,0,.14)",
            ':hover': {
                border: '1px solid #aaa',
                boxShadow: "0 4px 15px 0 rgba(0,0,0,.25)",
                cursor: 'pointer'
            }
        }

        return (
            <div className="Car" style={style}>
                <h3>Car name: {this.props.name}</h3>
                <p>Year: <strong>{this.props.year}</strong></p>
                <input
                    type="text"
                    onChange={this.props.onChangeName}
                    value={this.props.name}
                    className={inputClass.join(' ')}
                />
                <button onClick={this.props.onDelete}>Delete</button>
                {/*<button onClick={props.onChangeTile}>click</button>*/}
            </div>
        )
    }
}

// const Car = props => {
    // const inputClass = ['input']
    //
    // if (props.name !== '') {
    //     inputClass.push('green')
    // } else {
    //     inputClass.push('red')
    // }
    //
    // if (props.name.length > 4) {
    //     inputClass.push('bold')
    // }
    //
    // const style = {
    //     border: "1px solid #ccc",
    //     boxShadow: "0 4px 5px 0 rgba(0,0,0,.14)",
    //     ':hover': {
    //         border: '1px solid #aaa',
    //         boxShadow: "0 4px 15px 0 rgba(0,0,0,.25)",
    //         cursor: 'pointer'
    //     }
    // }

    // return (
    //     <div className="Car" style={style}>
    //         <h3>Car name: {props.name}</h3>
    //         <p>Year: <strong>{props.year}</strong></p>
    //         <input
    //             type="text"
    //             onChange={props.onChangeName}
    //             value={props.name}
    //             className={inputClass.join(' ')}
    //         />
    //         <button onClick={props.onDelete}>Delete</button>
    //         {/*<button onClick={props.onChangeTile}>click</button>*/}
    //     </div>
    // )
// }

export default Car
// export default Radium(Car)